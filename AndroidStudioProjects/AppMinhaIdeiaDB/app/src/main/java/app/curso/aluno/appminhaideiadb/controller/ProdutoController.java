package app.curso.aluno.appminhaideiadb.controller;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import java.util.List;

import app.curso.aluno.appminhaideiadb.api.AppUtil;
import app.curso.aluno.appminhaideiadb.datamodel.ProdutoDataModel;
import app.curso.aluno.appminhaideiadb.datasource.AppDataBase;
import app.curso.aluno.appminhaideiadb.model.Produto;

public class ProdutoController extends AppDataBase implements ICrud<Produto>{

    ContentValues dadoObjeto;

    //Construtor
    public ProdutoController(Context context) {
        super(context);
        Log.i(AppUtil.TAG, "ProdutoController: Conectada");
    }


    @Override
    public boolean incluir(Produto obj) {
        //criando empaço para armazenamento
        dadoObjeto = new ContentValues();

        //armazenando dados
        dadoObjeto.put(ProdutoDataModel.NOME, obj.getNome());
        dadoObjeto.put(ProdutoDataModel.FORNECEDOR, obj.getFornecedor());

        // enviando dados para o banco de dados
        return insert(ProdutoDataModel.TABELA, dadoObjeto);
    }

    @Override
    public boolean alterar(Produto obj) {
        //criando espaço para o armazenamento
        dadoObjeto = new ContentValues();

        //Armazenando dados
        dadoObjeto.put(ProdutoDataModel.ID, obj.getId());
        dadoObjeto.put(ProdutoDataModel.NOME, obj.getNome());
        dadoObjeto.put(ProdutoDataModel.FORNECEDOR, obj.getFornecedor());

        // Enviando dados para o armazenamento



        return false;
    }

    @Override
    public boolean deletar(int id) {
        return deletById(ProdutoDataModel.TABELA, id);
    }

    @Override
    public List<Produto> listar() {

        List<Produto> lista = null;
        return lista;
    }
}
