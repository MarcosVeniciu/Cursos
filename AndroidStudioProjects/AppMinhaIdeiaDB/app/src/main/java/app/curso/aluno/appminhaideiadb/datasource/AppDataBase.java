package app.curso.aluno.appminhaideiadb.datasource;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import app.curso.aluno.appminhaideiadb.api.AppUtil;
import app.curso.aluno.appminhaideiadb.datamodel.ClienteDataModel;
import app.curso.aluno.appminhaideiadb.datamodel.ProdutoDataModel;
import app.curso.aluno.appminhaideiadb.model.Cliente;


public class AppDataBase extends SQLiteOpenHelper {

    public static final String DB_NAME = "AppMinhaIdeia.sqlite";
    public static final int DB_VERSION = 1;

    SQLiteDatabase db;// Objeto banco de dados(onde os trecos vão ser salvos

    //Metodos
    public AppDataBase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db = getWritableDatabase();//Inicia o banco de dados com acesso para leitura e escrita

        Log.d(AppUtil.TAG, "AppDataBase: Criando Banco de dados");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Criando tabela de clientes
        db.execSQL(ClienteDataModel.criarTabela());//executa um comando SQL no banco de dados db
        Log.i(AppUtil.TAG, "onCreate: Tabela cliente criada");

        //Criando tabela de produtos
        db.execSQL(ProdutoDataModel.criarTabela());// cria a tabela no banco de dados execSQL
        Log.i(AppUtil.TAG, "onCreate: Tabela Produto criada");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Método para inserir dados no banco de dados
     * @return true caso tenha inserido com sucesso, false caso contrario
     */
    public boolean insert(String tabela, ContentValues dados){
        boolean resultado = false;
        db = getReadableDatabase();//abrir o banco de dados caso esteja fechado

        try {
            resultado = db.insert(tabela,null,dados) > 0; //se for maior do que zero então deu certo e retorna true

        }catch (Exception e){
            Log.e(AppUtil.TAG, "insert: "+tabela+" : "+e.getMessage());
        }

        return resultado;
    }


    /**
     * Método para deletar dados no banco de dados
     * @return true caso tenha deletado com sucesso, false caso contrario
     */
    public boolean deletById(String tabela, int id){
        boolean resultado = false;
        db = getReadableDatabase();//abrir o banco de dados caso esteja fechado

        try {
            // A ? indica que o valor que vai ficar no lugar dela é dinamico, e sera gerado pelo valor de new String [] {String.valueOf(id)}
            // new String [] {String.valueOf(id)} muda o id que é inteiro para string
            resultado = db.delete(tabela,"id = ?",new String [] {String.valueOf(id)}) > 0; //se for maior do que zero então deu certo e retorna true

        }catch (Exception e){
            Log.e(AppUtil.TAG, "delete: "+tabela+" : "+e.getMessage());
        }

        return resultado;
    }


    public boolean update(String tabela, ContentValues dados){
        db = getWritableDatabase();

        boolean retorno = false;

        try {
            // Recebe o nome da tabela
            // O ContentValue onde estão os novos dados
            // new String [] {String.valueOf(dados.get("id"))} converte o id para string
            // A condição, onde a ? sera trocado pelo id
            retorno = db.update(tabela,dados,"id = ?",new String [] {String.valueOf(dados.get("id"))}) > 0;
        }catch (Exception e){
            Log.d(AppUtil.TAG, "update: "+e.getMessage());
        }

        return retorno;
    }

    /**
     * Retorna a lista com todos os clientes.
     * @param tabela nome da tabela com os dados dos clientes
     * @return uma lista com todos os clientes
     */
    @SuppressLint("Range")
    public List<Cliente> getAllClientes(String tabela){
        List<Cliente> clientes = new ArrayList<>();
        Cliente obj;
        db = getReadableDatabase(); // abre o banco de dados apenas para leitura caso esteja fechado

        String sqlComando = "SELECT * FROM "+tabela;

        Cursor cursor;// Vai armazenar os dados retornado pela consulta ao banco de dados. Algo parecido com o foi feito com o ContentValues

        // rawQuery executa um comando do SQL
        // no selectionArgs tera a condição que ficara no lugar do ? no comando SQL
        cursor = db.rawQuery(sqlComando, null);


        //verifica se o cursor esta vazio
        if (cursor.moveToFirst()){// O cursor vai para o primeiro objeto da lista, se o cursor estiver vazio ele retorna false

            do {// passa por cada objeto salvo no cursor

                obj = new Cliente();//sempre cria um novo objeto para não ficar passando o mesmo endereço sempre. Não vai ficar alterando o que ta na lista

                //cursor.getInt() pega o dado que esta na coluna indicada pelo indice
                //cursor.getColumnIndex() informa qual é o indice da coluna que tem o mesmo nome que a string informada
                //ClienteDataModel.ID informa a string que tem o nome coluna que eu quero pegar o dado
                obj.setId(cursor.getInt(cursor.getColumnIndex(ClienteDataModel.ID)));
                obj.setNome(cursor.getString(cursor.getColumnIndex(ClienteDataModel.NOME)));
                obj.setEmail(cursor.getString(cursor.getColumnIndex(ClienteDataModel.EMAIL)));

                clientes.add(obj);

            }while (cursor.moveToNext());//se não houver mais pra onde ir ele retorna false
        }




        return clientes;
    }
}
