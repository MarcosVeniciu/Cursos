package app.curso.aluno.appminhaideiadb.datamodel;

public class ClienteDataModel {
    //Modelo objeto Relacional

    // Toda Classe data model tem essa estrutura

    // 1 - Atributo nome da tabela
    public static final String TABELA = "cliente";

    // 2 - Atributos um para um com os nomes dos campos
    public static final String NOME = "nome"; // são estaticos par que possam ser usados sem a necessidade de instanciar a classe
    public static final String EMAIL = "email";
    public static final String ID = "id";

    // 3 - Query para criar a tabela no banco de dados
    public static String queryCriarTabela = "";//é a string com o comando para criar a tabela

    // 4 - Método para gerar o Script para gerar a tabela
    public static String criarTabela(){//criada com static para poder ser usada sem precisar instancia-la

        queryCriarTabela +="CREATE TABLE "+TABELA+" (";
        queryCriarTabela +=ID+" INTEGER PRIMARY KEY AUTOINCREMENT, ";
        queryCriarTabela +=NOME+" TEXT, ";
        queryCriarTabela +=EMAIL+" TEXT ";
        queryCriarTabela +=")";

        return  queryCriarTabela;
    }
    // 5 - Queies para consultas gerais
}
