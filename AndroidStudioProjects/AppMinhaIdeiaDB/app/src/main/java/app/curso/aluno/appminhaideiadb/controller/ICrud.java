package app.curso.aluno.appminhaideiadb.controller;

import java.util.List;

//CRUD - Create Retrive(alterar) Update Delete
public interface ICrud<T>{ // Usar o <T> permite que os metodos da interface aceitem um objeto como parametro, qualquer tipo de objeto

    //Metodos para persistencia de dados no banco de dados

    //Incluir
    public boolean incluir(T obj);

    //Alterar
    public boolean alterar(T obj);

    //Deletar
    public boolean deletar(int id);

    //Listar
    public List<T> listar();
























}
