package app.curso.aluno.appminhaideiadb.controller;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import java.util.List;

import app.curso.aluno.appminhaideiadb.api.AppUtil;
import app.curso.aluno.appminhaideiadb.datamodel.ClienteDataModel;
import app.curso.aluno.appminhaideiadb.datamodel.ProdutoDataModel;
import app.curso.aluno.appminhaideiadb.datasource.AppDataBase;
import app.curso.aluno.appminhaideiadb.model.Cliente;


public class ClienteController extends AppDataBase implements ICrud<Cliente>{

    // Usa-se um ContentValues para que ao enviar para o banco de dados não precisse informar um tipo de objeto especifico.
    // Ao inves de criar dois metodos um  para cliente e outro produto, cria apenas um metodo para ContentValues
    ContentValues dadoObjeto;//objeto onde éra guardado os dados do objeto cliente para ser enviado para o banco de dados

    // Construtor
    public ClienteController(Context context) {
        super(context);//envia o contexto para o construtor da super classe AppDataBase
        Log.d(AppUtil.TAG, "ClienteController: Conectado");
    }


    @Override
    public boolean incluir(Cliente obj) {
        dadoObjeto = new ContentValues();// ele é formado por uma key para identificação e o dado

        // A key é o nome da coluna na tabela do banco de dados
        // Os dados estão no objeto cliente obj passado como parametro
        dadoObjeto.put(ClienteDataModel.NOME,obj.getNome());
        dadoObjeto.put(ClienteDataModel.EMAIL,obj.getEmail());

        //Envia os dados que estão salvos em dadosObjeto para o banco de dados com o INSERT INTO
        return insert(ClienteDataModel.TABELA, dadoObjeto);
    }

    @Override
    public boolean alterar(Cliente obj) {
        dadoObjeto = new ContentValues();// ele é formado por uma key para identificação e o dado

        // A key é o nome da coluna na tabela do banco de dados
        // Os dados estão no objeto cliente obj passado como parametro
        dadoObjeto.put(ClienteDataModel.ID, obj.getId());// para saber quem vai ser alterado
        dadoObjeto.put(ClienteDataModel.NOME,obj.getNome());
        dadoObjeto.put(ClienteDataModel.EMAIL,obj.getEmail());

        //Envia os dados que serão alterados no banco de dados com o UPDATE

        return update(ClienteDataModel.TABELA, dadoObjeto);
    }

    @Override
    public boolean deletar(int id) {
        return deletById(ClienteDataModel.TABELA, id);
    }

    @Override
    public List<Cliente> listar() {
       return getAllClientes(ClienteDataModel.TABELA);
    }
}
