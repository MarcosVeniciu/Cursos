package app.curso.aluno.appminhaideiadb.datamodel;

public class ProdutoDataModel {

    public static final String TABELA = "produto";//nome da tabela

    public static final String ID = "id";//o nome da coluna na tabela
    public static final String NOME = "nome";
    public static final String FORNECEDOR = "fornecedor";

    public static String queryCriarTabela = "";

    public static String criarTabela(){

        queryCriarTabela += "CREATE TABLE "+TABELA+" (";
        queryCriarTabela += ID+" INTEGER PRIMARY KEY AUTOINCREMENT, ";
        queryCriarTabela += NOME+" TEXT, ";
        queryCriarTabela += FORNECEDOR+" TEXT ";
        queryCriarTabela += ")";

        return queryCriarTabela;
    }












}
