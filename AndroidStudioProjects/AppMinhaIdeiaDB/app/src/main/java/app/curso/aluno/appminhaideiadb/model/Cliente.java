package app.curso.aluno.appminhaideiadb.model;

import android.util.Log;

import app.curso.aluno.appminhaideiadb.api.AppUtil;
import app.curso.aluno.appminhaideiadb.controller.ICrud;

public class Cliente {//essa classe possui apenas os metodos de set e get

    private int id;
    private String Nome;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String email;

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
