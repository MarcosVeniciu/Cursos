package app.curso.aluno.appminhaideiadb.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import app.curso.aluno.appminhaideiadb.R;
import app.curso.aluno.appminhaideiadb.api.AppUtil;
import app.curso.aluno.appminhaideiadb.controller.ClienteController;
import app.curso.aluno.appminhaideiadb.controller.ProdutoController;
import app.curso.aluno.appminhaideiadb.model.Cliente;

public class MainActivity extends AppCompatActivity {

    ClienteController clienteController;
    Cliente obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        clienteController = new ClienteController(this);//this é referente ao contexto atual

//        for (int i = 0; i < 50; i++) {
//            obj = new Cliente();
//
//            obj.setNome("Marcos_"+i);
//            obj.setEmail("marcos.veniciu_"+i+"@yahoo.com");
//
//            clienteController.incluir(obj);
//         }

        //lista os clientes no banco de dados
        for (Cliente cliente: clienteController.listar()) {
            Log.i(AppUtil.TAG, "onCreate: "+cliente.getId() +" "+cliente.getNome()+" "+cliente.getEmail());
        }


    }




}