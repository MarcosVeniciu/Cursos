package app.curso.aluno.ferramentalayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    //Apresentar texto
    TextView titulo;

    //Entrada de Dados
    EditText editnomeCompleto;

    Button btnConfirmar;
    ToggleButton btnOnOff;
    SwitchCompat swMostrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titulo = findViewById(R.id.textTitulo);
        editnomeCompleto = findViewById(R.id.editorNomeCompleto);
        btnConfirmar = findViewById(R.id.btnConfirmar);
        btnOnOff = findViewById(R.id.btnOnOff);
        swMostrar = findViewById(R.id.swMostrar);

        titulo.setText("Titulo de Teste");
        btnConfirmar.setText("Confirmar");
        editnomeCompleto.setHint("Informe o seu nome completo");


        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean dadosOk = true;

                if (TextUtils.isEmpty(editnomeCompleto.getText().toString())){//verifica se o usuario digitou o nome
                    editnomeCompleto.setError("Digite o Nome");
                    dadosOk = false;
                }

                if(dadosOk){
                    Toast.makeText(getBaseContext(),"Você Digitou: "+editnomeCompleto.getText(),Toast.LENGTH_LONG).show();
                }
            }
        });

        btnOnOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnOnOff.isChecked()){//se clicar no botão e ficar On
                    editnomeCompleto.setEnabled(false);// não sera possivel digitar um nome
                }else{// caso seja marcado com off
                    editnomeCompleto.setEnabled(true);// podera digitar um nome
                }

            }
        });

        swMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swMostrar.isChecked()){// se o switch esta marcado
                    titulo.setVisibility(View.GONE);//vai ocutar o titulo

                }else{
                    titulo.setVisibility(View.VISIBLE);//volta a exibir o titulo caso esta desabilitado
                    String novoTitulo = titulo.getText().toString().toUpperCase();//passa a string do titulo para maiusculo
                    titulo.setText(novoTitulo);//seta o novo titulo
                }
            }
        });

    }
}