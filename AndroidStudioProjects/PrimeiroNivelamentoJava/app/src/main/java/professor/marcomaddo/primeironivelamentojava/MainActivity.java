package professor.marcomaddo.primeironivelamentojava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import professor.marcomaddo.primeironivelamentojava.model.Cliente;

public class MainActivity extends AppCompatActivity {
    String TAG = "Teste";
    Cliente objetoCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        objetoCliente = new Cliente("marcos","marcos.veniciu@yahoo.com", "(31) 996916770", 24,true);

        Log.i(TAG, "onCreate: objeto nome: "+objetoCliente.getNome());
        
    }
}