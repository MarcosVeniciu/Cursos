package app.curso.aluno.auladenivelamentointerface;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    Cliente objCliente;
    Produto objProduto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        objCliente = new Cliente();
        objProduto = new Produto();

        //Cliente
        objCliente.setNome("marcos");
        objCliente.setEmail("marcos.veniciu@yahoo.com");

        objCliente.incluir();

        //Produto
        objProduto.setNome("HD 1TB");
        objProduto.setFornecedor("Dell");

        objProduto.listar();
        objProduto.deletar();

    }
}