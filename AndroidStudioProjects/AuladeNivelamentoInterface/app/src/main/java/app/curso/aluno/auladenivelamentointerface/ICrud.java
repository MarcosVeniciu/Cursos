package app.curso.aluno.auladenivelamentointerface;

//CRUD - Create Retrive(alterar) Update Delete
public interface ICrud {

    //Metodos para persistencia de dados no banco de dados

    //Incluir
    public void incluir();

    //Alterar
    public void alterar();

    //Deletar
    public void deletar();

    //Listar
    public void listar();
























}
