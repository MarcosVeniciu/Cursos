package app.daazi.aluno.appaulasp;//shared preference

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Aula_SP";
    private static final String PREF_NOME = "Aula_SP_pref";// nome do arquivo onde os dados serão salvos

    SharedPreferences sharedPreferences;//cria o arquivo onde sera salvos as coisas no celular
    SharedPreferences.Editor dados;// ele salva os dados que iram para a memoria do celular.


    String nomeProduto;
    int codigoProduto;
    float precoProduto;
    boolean estoque;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(TAG, "onCreate: Rodando");

        sharedPreferences = getSharedPreferences(PREF_NOME, Context.MODE_PRIVATE);//cria o arquivo com o nome e diz que é privado(so o aplicativo pode acessar)
        Log.i(TAG, "onCreate: Pasta Shared criada.");

        dados = sharedPreferences.edit();

        nomeProduto = "notebook";
        codigoProduto = 12345;
        precoProduto = 997.95f;
        estoque = true;

        // salva as informaçoes em dados e quanto as modificaçoes estiverem terminadas ele salvara os dados na memoria do celuar
        // uma formar de salvar as informaçoes sem precissar acessar a memoria do celular sempre que fizer alguma alteraçao.

        dados.putString("nomeProduto", nomeProduto);//escolhe uma chave para identificar e depois o que é para ser salvo
        dados.putInt("codigoProduto", codigoProduto);
        dados.putFloat("precoProduto", precoProduto);
        dados.putBoolean("estoque",estoque);

        // quando terminar as alteraçoes
        dados.apply();// salva os dados na memoria do celular


        //dados.clear();//apaga tudo o que ta salvo em dados
        //dados.apply();// aplica as mudanças no arquivo PREF_NOME

        //dados.remove("estoque");//remove apenas o dado que possui a key estoque
        //dados.apply();// aplica as mudanças no arquivo  PREF_NOME


        Log.d(TAG, "onCreate: Dados Recuperados.");
        Log.d(TAG, "onCreate: Produto "+sharedPreferences.getString("nomeProduto","Não exite"));
        Log.d(TAG, "onCreate: codigo "+sharedPreferences.getInt("codigoProduto", -1));
        Log.d(TAG, "onCreate: preco "+sharedPreferences.getFloat("precoProduto", -1.0f));
        Log.d(TAG, "onCreate: Tem no estoque "+sharedPreferences.getBoolean("estoque", false));
        Log.d(TAG, "onCreate: Tem no estoque "+sharedPreferences.getString("lampada", "Produto não existe"));
        // para fazer consulta deve informar a key do que esta buscando e adicionar uma resposta para o caso não encontrar o que procurava
        // sharedPreferences.getString("lampada", "Produto não existe"), nesse caso estava procurando por lampada, e se não tiver nada com
        // essa key ele vai retornar que o produto não existe
    }
}