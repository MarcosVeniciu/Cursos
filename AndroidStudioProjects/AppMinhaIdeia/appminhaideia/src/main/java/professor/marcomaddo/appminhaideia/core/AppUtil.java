package professor.marcomaddo.appminhaideia.core;

public class AppUtil {

    public final static String TAG = "Teste";
    // final - significa que o valor da variavel nuca vai mudar
    // static - significa que pode ser usada sem a necessidade de instanciar a classe ( new Apputil() )

    public static String versaoDoAplicativo(){
        return "12.1";
    }

}
