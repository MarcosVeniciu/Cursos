package professor.marcomaddo.appminhaideia.model;

public class Cliente {
    //atributos - mone, email, telefone, idade, sexo
    // são as caracteristicas que o objetos representado pela classe possui
    private String nome;
    private String email;
    private String telefone;
    private int idade;
    private boolean sexo;

    // construtor
    //quando um objeto dessa classe so pode ser criado, se os atributos no construtor
    //forem reenchidos. Se não houver um construtor os objetos podem ser criados
    //com seus atributos vazios, pelo construtor padão.
    // Comando da IDE dir. mouse > generete > contrutor
    public Cliente(String nome, String email, String telefone, int idade, boolean sexo) {
        this.nome = nome;
        this.email = email;
        this.telefone = telefone;
        this.idade = idade;
        this.sexo = sexo;
    }


    //metodos de acesso - get e set
    // Servem para acessar os dados da classe: coletar(get) os dados dos atributos da classe
    // e para configurar/alterar(set) os dados dos atributos da classe
    // Comando da IDE dir. mouse > generete > getter and setter

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }
}
