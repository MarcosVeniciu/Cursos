package professor.marcomaddo.appminhaideia.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import professor.marcomaddo.appminhaideia.R;
import professor.marcomaddo.appminhaideia.core.AppUtil;

public class MainActivity extends AppCompatActivity {

    TextView txtNome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d(AppUtil.TAG, "onCreate: Tela principal carregada");

        //coletar informaçoes de outra tela
        Bundle bundle = getIntent().getExtras();

        txtNome = findViewById(R.id.txtNome);// o meu objeto txtNome vai ser associado a um texView com o id de TxtNome que esta no layout
        txtNome.setText("Ben vindo : "+ bundle.getString("nome"));

        Log.d(AppUtil.TAG,"onCreate: nome: "+bundle.getString("nome"));
        Log.d(AppUtil.TAG,"onCreate: nome: "+bundle.getString("email"));






    }

}