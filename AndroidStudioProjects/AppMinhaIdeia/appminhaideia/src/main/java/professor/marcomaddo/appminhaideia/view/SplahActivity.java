package professor.marcomaddo.appminhaideia.view;



import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.TextView;

import professor.marcomaddo.appminhaideia.R;
import professor.marcomaddo.appminhaideia.core.AppUtil;
import professor.marcomaddo.appminhaideia.model.Cliente;

public class SplahActivity extends AppCompatActivity {

    int tempoDeEspera = 1000 * 10; //tempo em milisegundos

    Cliente objCliente;
    TextView versao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {// esse é o metodo principal da classe
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splah);

        versao = findViewById(R.id.txtAppVersion);//faz a associação do id o texview do layout com o versão
        versao.setText(AppUtil.versaoDoAplicativo());







        Log.d(AppUtil.TAG, "onCreate: Tela splah carregada");

        trocarTela();//padrão para nomear metodo primeira palavra minuscula e segunda palavra com maiuscula
    }


    private void trocarTela() {
        Log.d(AppUtil.TAG, "trocartela: Iniciado");

        //esperar um tempo para trocar de tela com um dalay de tempo de espera
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Log.d(AppUtil.TAG, "run: Eperando um tempo.");

                objCliente = new Cliente(
                        "marcos veniciu de sa barbalho",
                        "marcos.veniciu@yahoo.com",
                        "(31) 996916770",
                        24,
                        true
                );


                //Intenst é uma classe. Representa uma intensção, nesse caso trocar de tela
                Intent trocarDeTela = new Intent(SplahActivity.this, MainActivity.class);


                /* Para enviar dados de uma tela para a outra atraves da intent*/
                Bundle bundle = new Bundle();
                bundle.putString("nome",objCliente.getNome()); // vai enviar o nome para a outra tela
                bundle.putString("email",objCliente.getEmail());

                trocarDeTela.putExtras(bundle);



                // agora vou executar a intensão, e ela pede a intensção como parametro
                startActivity(trocarDeTela);
                //para nao executar novamente
                finish();
            }
        }, tempoDeEspera);




    }


}