package app.curso.aluno.appbancodedadosmeusclientes;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

//Para ter suporte ao SQLite
public class AppDataBase extends SQLiteOpenHelper {

    private static final String TAG = "db";


    public static final String DB_NAME = "MeusClientes.sqlite";
    public static final int DB_VERSION = 1;

    SQLiteDatabase db;//banco de dados

    public AppDataBase(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        db = getWritableDatabase();//inicia o banco de dados para escrita e leitura
        Log.i(TAG, "AppDataBase: instanciado");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //script para gerar a tabela
        String tabelaCliente = "CREATE TABLE cliente " +
                "(id INTEGER PRIMARY KEY AUTOINCREMENT," +
                " nome TEXT," +
                " salario REAL," +
                " preco NUMERIC," +
                " idade INTEGER," +
                " ativo INTEGER," +
                " dataCadastro TEXT," +
                " horaCadastro TEXT)";

        try {
            db.execSQL(tabelaCliente);//executar o comando para criar a tabela tabelaCliente no banco de dados DB_NAME
            Log.i(TAG, "onCreate: criado tabela");
        }catch (SQLException e){
            Log.e(TAG, "onCreate: "+e.getLocalizedMessage());//caso tenha algum erro mostrara qual é
        }



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
